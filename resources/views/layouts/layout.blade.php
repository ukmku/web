<?php

// use Illuminate\Http\Request;
// use GuzzleHttp;
// use GuzzleHttp\Client;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Session;
// require '/vendor/autoload.php';

// $client = new GuzzleHttp\Client();
// $res = $client->request('GET', 'http://178.128.222.175:8080/api/v1/users/profile', [
//     'headers' => [
//       'token' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1vbmlrYUBnbWFpbC5jb20iLCJwYXNzd29yZCI6IiQyYiQxMCRxcVZ4b3M5cjNIZEQ4dklRcjVBcWYuT2ouQXRpUlhNZ1NlN2hTLll1cnVGNXRSNzdlLmhyNiIsImlhdCI6MTU5ODE5OTM1NH0.c5qycybzYjTungzDzxJC4RKzM-Tt1rDZuKQJH_POHZ0',
//       ]
// ]);
// $post = json_decode($res->getBody(), true);

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>UKMKU</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="assets/css/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="assets/images/ukmku.png" />

</head>

<body>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="assets/images/ukmku.png" alt="logo" /></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="mdi mdi-menu"></span>
        </button>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <div class="nav-profile-img">
                <img src="assets/images/logo.png" alt="image">
                <span class="availability-status online"></span>
              </div>
              <div class="nav-profile-text">
                <p class="mb-1 text-black">Nama UKM</p>
              </div>
            </a>
            <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="/logout">
                <i hre class="mdi mdi-logout mr-2 text-primary"></i> Signout </a>
            </div>
          </li>
          <!-- <li class="nav-item d-none d-lg-block full-screen-link">
            <a class="nav-link">
              <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
            </a>
          </li> -->
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="mdi mdi-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <div class="nav-profile text-center">
            <img src="assets/images/logo.png" alt="profile">
          </div>
          <!-- <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
              <div class="nav-profile-image">
                <img class="text-center" src="assets/images/logo.png" alt="profile">
              </div>
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="/">
              <span class="menu-title">Dashboard</span>
              <i class="mdi mdi-home menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/create">
              <span class="menu-title">Kelola UKMKU</span>
              <i class="mdi mdi-contacts menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/galery">
              <span class="menu-title">Foto & Video</span>
              <i class="mdi mdi-folder-multiple-image menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/tentang">
              <span class="menu-title">Tentang UKMKU</span>
              <i class="mdi mdi-emoticon menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/register">
              <span class="menu-title">Register</span>
              <i class="mdi mdi-emoticon menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/image">
              <span class="menu-title">Upload Image</span>
              <i class="mdi mdi-emoticon menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/masterUkm">
              <span class="menu-title">Master UKM</span>
              <i class="mdi mdi-emoticon menu-icon"></i>
            </a>
          </li>
          {{-- <li class="nav-item">
              <a class="nav-link" href="pages/forms/basic_elements.html">
                <span class="menu-title">Forms</span>
                <i class="mdi mdi-format-list-bulleted menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/charts/chartjs.html">
                <span class="menu-title">Charts</span>
                <i class="mdi mdi-chart-bar menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/tables/basic-table.html">
                <span class="menu-title">Tables</span>
                <i class="mdi mdi-table-large menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages">
                <span class="menu-title">Sample Pages</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-medical-bag menu-icon"></i>
              </a>
              <div class="collapse" id="general-pages">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/blank-page.html"> Blank Page </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Login </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/register.html"> Register </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/error-404.html"> 404 </a></li>
                  <li class="nav-item"> <a class="nav-link" href="pages/samples/error-500.html"> 500 </a></li>
                </ul>
              </div>
            </li> --}}
        </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <!--content-->
          @yield('content')
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 By Imam Mahudi <a href="https://www.bootstrapdash.com/" target="_blank"></a>. All rights reserved.</span>
            </div>
          </footer>
          <!--end content-->
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
    </div>
    <!-- page-body-wrapper ends -->
  </div>

  <!-- container-scroller -->
  <!-- plugins:js -->

  <!-- <script src="assets/vendors/js/vendor.bundle.base.js"></script> -->

  <!-- endinject -->
  <!-- Plugin js for this page -->
  <!-- <script src="assets/vendors/chart.js/Chart.min.js"></script> -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="assets/js/off-canvas.js"></script>
  <script src="assets/js/hoverable-collapse.js"></script>
  <script src="assets/js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page -->
  <script src="assets/js/dashboard.js"></script>
  <script src="assets/js/todolist.js"></script>
  <!-- End custom js for this page -->
</body>

</html>