@extends('layouts.layout')

@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
<div class="col-12">
  <div class="card">
    <div class="card-body">
      <div class="card-title">
        <h4>Kelola UKM</h4>
        <p>Tambah & Update Data</p>
      </div>
      <form class="form-sample" action="/createpost" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
          <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Logo </label>
              <div class="input-group col-xs-12">
                <span class="input-group-append">
                  <input class="file-upload-browse btn bg-blue text-white" name="image" type="file">
                  {{-- <input type="file" class="form-control-file" id="exampleFormControlFile1"> --}}
                </span>
              </div>
            </div>
          </div>
          {{-- @foreach ($response as $item)    --}}
            <div class="col-md-6">
            <div class="form-group">
            <label class="col-form-label">Contact Person</label>
            <input name="contact_person" type="text" class="form-control" value=""/>
            </div>
          </div>  
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Nama UKM </label>
              <input name="nama_ukm" type="text" class="form-control" placeholder="Masukkan nama ukm" />
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Bidang UKM</label>
              <select class="form-control" name="bidang_ukm" id="bidang_ukm">
                <option value="Kerohanian">Kerohanian</option>
                <option value="Olahraga">Olahraga</option>
                <option value="Seni Budaya">Seni Budaya</option>
              </select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label class="col-form-label">Latar Belakang / Deskripsi</label>
               <textarea class="form-control" id="exampleTextarea1" name="ltrblkg" rows="4"></textarea>
            </div>
          </div>
          <div class="col-md-12">
            <label class="col-form-label">Media Sosial</label>
            <p class="note-text">Masukkan link sosial media UKM di bawah ini</p>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="instagram">Instagram</label>
                  <input type="text" name="ig" class="col-md-9 form-control" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="facebook">Facebook</label>
                  <input type="text" name="fb" class="col-md-9 form-control" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="twitter">Twitter</label>
                  <input type="text" name="twitter" class="col-md-9 form-control" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="website">Website</label>
                  <input type="text" name="web" class="col-md-9 form-control" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="youtube">Youtube</label>
                  <input type="text" name="ytb" class="col-md-9 form-control" />
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-md-3 col-form-label" for="youtube">email</label>
                  <input type="text" name="email" class="col-md-9 form-control" />
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">List Kegiatan</label>
               <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">List Prestasi</label>
              <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="col-form-label">Jadwal Pertemuan</label>
           <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
            </div>
          </div>
           {{-- @endforeach --}}
        </div>
        <button type="submit" class="btn bg-blue text-white mr-2">Submit</button>
        <a href="/dashboard" class="btn btn-light">Cancel</a>
      </form>
    </div>
  </div>
</div>
@endsection
