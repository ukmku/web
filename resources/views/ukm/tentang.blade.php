@extends('layouts.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script> -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

<div class="col-12">
    <div class="card">
        <div class="card-body">
            <div class="card-title">
                <center>
                    <h2>Meet Our Team</h2>
                    <h3 class="text-blue">UKMKU</h3>
                </center>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_irvan.png" alt="">
                <div class="tulisan">
                    <h6>Irvan Alfaridzi Dwi P.</h6>
                    <p>Product Owner</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_denandra.png" alt="">
                <div class="tulisan">
                    <h6>Denandara Prasetya L. P.</h6>
                    <p>Lead Developer</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_bastian.png" alt="">
                <div class="tulisan">
                    <h6>Sebastianus Sembara</h6>
                    <p>Back End Developer</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_imam.png" alt="">
                <div class="tulisan">
                    <h6>M.Imam Mahudi R.</h6>
                    <p>Front End Developer</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_bella.png" alt="">
                <div class="tulisan">
                    <h6>Bella Ramadhanty</h6>
                    <p>UI/UX Designer</p>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <img class="rounded-circle img-foto-tentang" src="assets/images/photo_femmy.png" alt="">
                <div class="tulisan">
                    <h6>Femmy Liana P.</h6>
                    <p>UI/UX Designer</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection