@extends('layouts.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script> -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

<div class="card">
    <div class="card-body">
        <form id="" action="" method="POST" class="forms-sample">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-9">
                    <h4>Master UKM</h4>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary bg-blue float-right" data-toggle="modal" data-target="modalCreate">
                        Tambah Data UKM
                    </button>
                </div>
                <div class="col-md-2 btn-cetak">
                    <input class="form-control bg-blue text-white" type="button" value="PDF">
                </div>
                <div class="col-md-2 btn-cetak">
                    <input class="form-control bg-blue text-white" type="button" value="EXCEL">
                </div>
            </div>
            <hr>
            <div class="forms-sample">
                <table id="myTable" class="">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama UKM</th>
                            <th>Bidang UKM</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th style="width: 30%;">Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>UKM01</td>
                            <td>Dinamika Java Network</td>
                            <td>Penalaran</td>
                            <td>javanetwork</td>
                            <td><span>lihat password</span></td>
                            <td>
                                <input type="button" class="btn bg-blue text-white" value="Detail">
                                <input type="button" class="btn bg-orange text-white" value="Edit">
                                <input type="button" class="btn bg-red text-white" value="Hapus" data-toggle="modal" data-target="#modalDelete">
                            </td>
                        </tr>
                        <tr>
                            <td>UKM01</td>
                            <td>Dinamika Java Network</td>
                            <td>Penalaran</td>
                            <td>javanetwork</td>
                            <td><span>lihat password</span></td>
                            <td>
                                <input type="button" class="btn bg-blue text-white" value="Detail">
                                <input type="button" class="btn bg-orange text-white" value="Edit">
                                <input type="button" class="btn bg-red text-white" value="Hapus" data-toggle="modal" data-target="#modalDelete">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="modalDelete" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin ingin menghapus data tersebut?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary bg-blue">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- //Modal Delete -->

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@endsection