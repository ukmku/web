@extends('layouts.layout')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script> -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>


<div class="col-12">
  <h4>Foto dan Video</h4>
  <p>Kelola foto dan video UKM</p>
</div>

<div class="col-12">
  <div class="card">
    <div class="card-body">
      <div class="card-title">
        <div class="row">
          <div class="col-md-9">
            <h4>Foto</h4>
          </div>
          <div class="col-md-3">
            <button class="btn bg-blue text-white float-right">Tambah Foto</button>
          </div>
        </div>
      </div>
      <hr>
      <div class="form-sample">
        <table id="myTable" class="">
          <thead>
            <tr>
              <th>Foto</th>
              <th>Judul</th>
              <th>Keterangan</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><img class="img-foto" src="assets/images/esjeruk.jpg" alt=""></td>
              <td>Seminar Android: Introducing to djfbskfdj</td>
              <td>UI/UX Designer</td>
              <td>
                <input type="button" class="form-control bg-blue text-white" value="Edit Profile">
                <input type="button" class="form-control bg-red text-white" value="Hapus" data-toggle="modal" data-target="#modalDelete">
              </td>
            </tr>
            <tr>
              <td><img class="img-foto" src="assets/images/esteh.jpg" alt=""></td>
              <td>Seminar Website: Introducing to djfbskfdj</td>
              <td>UI/UX Designer</td>
              <td>
                <input type="button" class="form-control bg-blue text-white" value="Edit Profile">
                <input type="button" class="form-control bg-red text-white" value="Hapus" data-toggle="modal" data-target="#modalDelete">
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <hr>
    </div>
  </div>
</div>

<div class="card-video col-12">
  <div class="card">
    <div class="card-body">
      <div class="card-title">
        <h4>Video</h4>
      </div>
      <hr>
      <div>
        <div class="col-md-12">
          <div class="form-group">
            <label class="col-form-label">Link Video Prodile UKM </label>
            <div class="row">
              <div class="col-md-9">
                <input id="nama_ukm" type="text" class="form-control" placeholder="Masukkan link video profile UKM" />
              </div>
              <div class="col-md-3">
                <button type="submit" class="form-control bg-blue text-white">Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="modalDelete" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin ingin menghapus data tersebut?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary bg-blue">Ok</button>
            </div>
        </div>
    </div>
</div>
<!-- //Modal Delete -->

<script>
  $(document).ready(function() {
    $('#myTable').DataTable();
  });
</script>
@endsection
