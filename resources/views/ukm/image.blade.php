@extends('layouts.layout')

@section('content')

<form action="/imagepost" method="POST" class="md-form" enctype="multipart/form-data">
    {{ csrf_field() }}
  <div class="file-field">
    <div class="btn btn-primary btn-sm float-left">
      <span>Choose file</span>
      <input type="file" name="image">
    </div>
    <div class="file-path-wrapper">
      <input class="file-path validate"  type="text" placeholder="Upload your file">
    </div>
  </div>
  <button type="submit">Simpan</button>
</form>

@endsection