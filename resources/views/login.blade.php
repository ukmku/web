<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>UKMKU</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../../assets/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../assets/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- Plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <!-- endinject -->
  <!-- Layout styles -->
  <link rel="stylesheet" href="../../assets/css/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="assets/images/ukmku.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth">
        <div class="row flex-grow">
          <!-- <div class="col-lg-4 mx-auto">
            <img class="bg-login" src="../../assets/images/bg-login.png" alt="">
          </div> -->
          <div class="col-lg-8 mx-auto">
            <div class="auth-form-light text-left p-5">
              <div class="brand-logo">
                <img src="../../assets/images/ukmku.png">
              </div>
              <h4>Selamat datang di UKMKU</h4>

              @if(\Session::has('alert'))
              <div class="alert alert-danger">
                <div>{{Session::get('alert')}}</div>
              </div>
              @endif
              @if(\Session::has('alert-success'))
              <div class="alert alert-success">
                <div>{{Session::get('alert-success')}}</div>
              </div>
              @endif

              <form class="pt-3" action="/masuk" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" name="username" id="email" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Password">
                </div>
                <div class="mt-3">
                  <button type="submit" id="btnlogin" class="btn btn-block bg-blue text-white btn-lg font-weight-medium auth-form-btn">SIGN IN</button>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" class="form-check-input"> Keep me signed in </label>
                  </div>
                  <a href="#" class="auth-link text-black">Forgot password?</a>
                </div>
                <!-- <img class="image-login" src="../../assets/images/image_login.png" alt=""> -->
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="../../assets/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- inject:js -->
  <script src="../../assets/js/off-canvas.js"></script>
  <script src="../../assets/js/hoverable-collapse.js"></script>
  <script src="../../assets/js/misc.js"></script>
  <!-- endinject -->
</body>

</html>

{{-- function login --}}
{{-- <script src="assets/vendors/js/script.js"></script>
            <script type="text/javascript"> 
              $(document).ready(function () {
                  $('#btnlogin').on('click', function () {
                   let data = {
                  "email" : $('#email').val(),
                  "password" : $('#password').val()
                };
                console.log(data)
                $.ajax({
                  type: "POST",
                  url:'http://178.128.222.175:8080/api/v1/auth/signin',
                  data: data,
                  dataType: 'JSON',
                  success: function (response){
                    console.log(response)
                  },
                  error: function(error){
                    console.log(error)
                    alert("data tidak disimpan");
                  }
                })
                });
              });
            </script> --}}