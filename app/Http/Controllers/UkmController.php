<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use GuzzleHttp;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class UkmController extends Controller
{
    public function index(Request $request)
    {

        // $data = Http::get('http://178.128.222.175:8080/api/v1/communities/profile')->json();
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = session::get('token');
            $client = new GuzzleHttp\Client([
                'headers' => ['Authorization' => 'Bearer ' . $token[0]],
            ]);
            // $request = $client->get('https://ukmku.sembara.site/api/v1/communities/profile');
            // $response = $request->getBody();
            return view('ukm.index');
        }
    }

    public function create(Request $request)
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $token = session::get('token');
            $client = new GuzzleHttp\Client([
                'headers' => ['Authorization' => 'Bearer ' . $token[0]],
            ]);
            $request = $client->get('https://ukmku.sembara.site/api/v1/communities/profile');
            $response = $request->getBody();
            return view('ukm.create', ['response' => $response]);
        }
    }


    public function createpost(Request $request)
    {

        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,svg,gif'
        ]);

        // $path = $request->file('image')->store('');
        // $base64 = 'data:image/' . base64_encode($path);
        $path = $request->file('image')->store('public/img');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents('D:88189.jpg');
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $token = session::get('token');
        $client = new GuzzleHttp\Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->POST('http://ukmku.sembara.site/api/v1/communities', [
            'json' => [
                [
                    "logo" => $request->$base64,
                    "name" => $request->nama_ukm,
                    "category" => $request->bidang_ukm,
                    "description" => $request->ltrblkg,
                    "schedule" => $request->jdwl,
                    "activities" => $request->listkg,
                    "achievements" => $request->listp,
                    "contactPerson" => $request->contact_person,
                    "email" => $request->email,
                    "instagram" => $request->ig,
                    "twitter" => $request->twitter,
                    "youtube" => $request->ytb,
                    "facebook" => $request->fb,
                    "website" => $request->web,
                ],
            ],
        ]);
        $data = json_decode($response->getBody()->getContents());
        return view('ukm.create', ['data' => $data]);
    }

    public function getdataprofile()
    {
        $token = 'token';
        $client = new GuzzleHttp\Client();
        $response = $client->request('GET', 'http://ukmku.sembara.site/api/v1/communities/profile', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'json' => []
        ]);
    }

    public function galery()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('ukm.galery');
        }
    }
    public function register()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('ukm.register');
        }
    }
    public function tentang()
    {
        return view('/ukm/tentang');
    }
    public function login()
    {
        return view('login');
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->flush();
        return redirect('/login');
    }
    public function Postregister(Request $request)
    {
        $token = session::get('token');
        $client = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token[0]],
        ]);
        $response = $client->request('POST', 'https://ukmku.sembara.site/api/v1/communities', [
            'json' => [
                "username" => $request->username,
                "password" => $request->password,
                "passwordConfirmation" => $request->passwordulang,
                "name" => $request->nama_ukm,
                "category" => $request->category,
            ],
        ]);
        $data = json_decode($response->getBody()->getContents());
        return redirect('/register');
    }
    public function masuk(Request $request)
    {
        try {
            $client = new Client([
                'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            ]);
            $response = $client->request('POST', 'https://ukmku.sembara.site/api/v1/auth/community/signin', [
                'json' => [
                    "username" => $request->username,
                    "password" => $request->password,
                ],
            ]);

            $data = json_decode($response->getBody()->getContents());

            if (isset($data->data->token)) { //apakah email tersebut ada atau tidak
                Session::push('token', $data->data->token);
                Session::push('login', TRUE);
                return redirect('/');
            } else {
                return redirect('login', ['data' => $data])->with('alert', 'Terjadi Kesalahan Hubungi Administrator!');
            }
        } catch (\Throwable $th) {
            // dd($th);
            return redirect('login')->with('alert', 'Password atau Email, Salah!');
        }
    }
    public function image()
    {
        return view('ukm.image');
    }
    public function imagepost(Request $request)
    {
        $path = $request->file('image')->store('public/img');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents('D:88189.jpg');
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        // $uploadedFile = $request->file('image');
        // $path = $uploadedFile->store('public/files');
        // $file = File::create([
        //     'title' => $request->title ?? $uploadedFile->getClientOriginalName(),
        //     'filename' => $path
        // ]);
        // dd($path);
    }
    public function masterUkm()
    {
        return view('ukm.masterUkm');
    }
}
