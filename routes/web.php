<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route UKM
Route::get('/', 'UkmController@index');
Route::get('/create', 'UkmController@create');
Route::post('/createpost', 'UkmController@createpost');
Route::get('/galery', 'UkmController@galery');
Route::get('/register', 'UkmController@register');
Route::get('/tentang', 'UkmController@tentang');
Route::get('/login', 'UkmController@login');
Route::get('/logout', 'UkmController@logout');
Route::post('/Postregister', 'UkmController@Postregister');
Route::post('/masuk', 'UkmController@masuk');
Route::get('/image', 'UkmController@image');
Route::post('/imagepost', 'UkmController@imagepost');
Route::get('/masterUkm', 'UkmController@masterUkm');

// Route Admin
Route::get('/dashboard', 'AdminController@index');
